@extends('layout/main')

@section('title', 'Detail Mahasiswa')

@section('container')
<div class="container">
  <div class="row">
    <div class="col-6">
      <h1 class="mt-3">Detail Mahasiswa</h1>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$student->nama}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$student->email}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">{{$student->jurusan}}</h6>
                <a href="{{ $student->id }}/edit" class="btn btn-primary">Edit</button>
                <form action="{{ $student->id }}" method="post" >
                  @method('delete')
                  @csrf
                  <button type="submit" class="btn btn-danger">Delete</button>
                </form>
                <a href="/students" class="card-link">Kembali</a>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection